import { TestBed } from '@angular/core/testing';

import { TestLibPqService } from './test-lib-pq.service';

describe('TestLibPqService', () => {
  let service: TestLibPqService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestLibPqService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
