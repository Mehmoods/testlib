import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestLibPqComponent } from './test-lib-pq.component';

describe('TestLibPqComponent', () => {
  let component: TestLibPqComponent;
  let fixture: ComponentFixture<TestLibPqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestLibPqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestLibPqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
