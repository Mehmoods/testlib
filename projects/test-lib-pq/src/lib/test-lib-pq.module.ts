import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TestLibPqComponent } from './test-lib-pq.component';

@NgModule({
  declarations: [
    TestLibPqComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TestLibPqComponent
  ]
})
export class TestLibPqModule { }
