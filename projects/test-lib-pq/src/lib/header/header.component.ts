import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'lib-test-header-pq',
    template: `
      <h1>
       {{label}}
      </h1>
  `,
    styleUrls: ['./../test-lib-pq.scss']
})
export class HeaderComponent {

    @Input() label: any;

    constructor() { }

}
