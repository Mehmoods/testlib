import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'lib-test-lib-pq',
  template: `
      <h1>
        Data is Klyas!
      </h1>

      <pre> {{data | json}} </pre>
      <p>{{testInput}} </p>

      <ol>
        <li> Li </li>
        <li> Ci </li>
        <li> Center </li>
        <li> Dom! </li>
      </ol>
  `,
  styleUrls: ['test-lib-pq.scss']
})
export class TestLibPqComponent implements OnInit {

  @Input() data: any;
  @Input() testInput: any;

  constructor() { }

  ngOnInit(): void {

  }

}
