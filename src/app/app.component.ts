import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'publish-test';

  data = [
    {
      key: 'add',
      isCreate: true,
      className: 'btn',
      template: '+'
    },
    {
      key: 'edsdit',
      isUpdate: true,
      template: '<i class="fa fa-pencil"></i>',
      className: 'kklir edit'
    },
    {
      key: 'delete',
      template: '<i class="fa fa-trash-o"></i>',
      className: 'kklir delete'
    }
  ];
}
